FROM ushahidi/node-ci:node-10-gulp-4 AS build

RUN mkdir -p /var/app
WORKDIR /var/app
COPY package.json .
RUN npm-install-silent.sh

COPY . ./
ARG TX_USERNAME
ARG TX_PASSWORD
RUN TX_USERNAME="${TX_USERNAME}" TX_PASSWORD="${TX_PASSWORD}" gulp build


FROM nginx:alpine as final
COPY --from=0 /var/app/server/www/* /usr/share/nginx/html/
EXPOSE 80
